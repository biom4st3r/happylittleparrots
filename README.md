# Happy Little Parrots

## Full list of features:
- Dropped parrots turn into regular non-exploding parrots
    - Even when dropped by breaking chests
- Different colored parrots behave differently when thrown
    <details>

    - Blue - gotta go fast

    - Green - never damages blocks

    - Gray - fall slower

    - Red - explode harder

    </details>

- Feeding(right clicking) a parrot gunpowder in your inventory will increase their boom factor
    - BOM -> BOOM -> BOOOM -> BOOOOM
    - Anything beyond BOOOOM is wasted...right?
- Hitting parrots with birdnets will pull them into your inventory
- Powdered Parrots don't explode then thrown under some conditions
    <details>
    Mostly when they hit #happylittleparrots:soft_blocks: like wool, leaves, slime, honey, and beds. Also when they are in water.
    </details>
- A thrown powdered parrot who doesn't explode loses all their boom factor
- Recipe
```
S = String
I = stick
 _____
|S S S|
|S S S|
|  I  |
 -----
```
