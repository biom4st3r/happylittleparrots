package biom4st3r.mods.happylittleparrots;

import biom4st3r.libs.biow0rks.PooledItemComponent.ComponentKey;
import biom4st3r.mods.happylittleparrots.entity.ThrownParrotEntity;
import biom4st3r.mods.happylittleparrots.items.BirdNetItem;
import biom4st3r.mods.happylittleparrots.items.HeldParrot;
import biom4st3r.mods.happylittleparrots.items.IParrotItemComponent;
import biom4st3r.mods.happylittleparrots.items.IParrotItemComponent.Power;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleFactory;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.GameRules;
import net.minecraft.world.GameRules.BooleanRule;

public class Reg {

    public static final ComponentKey<IParrotItemComponent> KEY_HELD_PARROT = ComponentKey.of(IParrotItemComponent.class, "happylittleparrots:parrot", IParrotItemComponent::new);
    public static final Item ITEM_BIRD_NET = new BirdNetItem();
    public static final Item ITEM_HELD_PARROT = new HeldParrot();
    public static final GameRules.Key<BooleanRule> THROWN_PARROTS_DO_BLOCK_DAMAGE = GameRuleRegistry.register("thrownParrotsDoBlockDamage", GameRules.Category.MISC, GameRuleFactory.createBooleanRule(true));;

    public static final EntityType<ThrownParrotEntity> ENTITY_THROWN_PARROT = FabricEntityTypeBuilder
        .<ThrownParrotEntity>create()
        .trackRangeChunks(5)
        .dimensions(new EntityDimensions(0.5F, 0.9F, true))
        .entityFactory((type,world) -> {
            return new ThrownParrotEntity(world, -1, "", Power.NO, null);
        })
        .build();

    private static Identifier id(String s) {
        return new Identifier(ModInit.MODID, s);
    }

    public static void classLoad() {
        Registry.register(Registry.ENTITY_TYPE, id("thrown_parrot"), ENTITY_THROWN_PARROT);
        Registry.register(Registry.ITEM, id("birdnet"), ITEM_BIRD_NET);
        Registry.register(Registry.ITEM, id("held_parrot"), ITEM_HELD_PARROT);
    }
}
