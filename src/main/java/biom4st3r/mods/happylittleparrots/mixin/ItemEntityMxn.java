package biom4st3r.mods.happylittleparrots.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import biom4st3r.mods.happylittleparrots.items.HeldParrot;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

@Mixin(ItemEntity.class)
public abstract class ItemEntityMxn extends Entity {

    public ItemEntityMxn(EntityType<?> type, World world) {
        super(type, world);
    }

    @Inject(at = @At("TAIL"), method = "setStack(Lnet/minecraft/item/ItemStack;)V")
    private void replaceWithBird(ItemStack stack, CallbackInfo ci) {
        HeldParrot.removeItemEntity((ItemEntity)(Object)this);
    } 

    @Shadow
    public ItemStack getStack(){ return null;}
}
