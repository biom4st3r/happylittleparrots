package biom4st3r.mods.happylittleparrots;

import java.io.IOException;
import java.io.InputStreamReader;

import biom4st3r.mods.happylittleparrots.entity.ThrownParrotEntityRenderer;
import biom4st3r.mods.happylittleparrots.items.HeldParrotDyamicRenderer;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.model.ModelLoadingRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.BuiltinItemRendererRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.minecraft.client.render.model.UnbakedModel;
import net.minecraft.client.render.model.json.JsonUnbakedModel;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.util.ModelIdentifier;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;

/**
 * ModInitClient
 */
public class ModInitClient implements ClientModInitializer {

    // private static RenderLayer layer = 

    private ModelIdentifier[] modelIdentifiers = {
        new ModelIdentifier(ModInit.MODID + ":birdnet#inventory"),
        new ModelIdentifier(ModInit.MODID + ":held_parrot#inventory")
    };

    private static UnbakedModel getBirdNet(ResourceManager manager) throws IOException {
        return JsonUnbakedModel.deserialize(new InputStreamReader(manager
                            .getResource(new Identifier(ModInit.MODID, "models/item/birdnet.json")).getInputStream()));
    }

    public static ModelTransformation STOLEN_TRANSFORM = null;
    // public static ModelOverrideList STOLEN_OVERRIDES = null;

    @Override
    public void onInitializeClient() {
        BuiltinItemRendererRegistry.INSTANCE.register(Reg.ITEM_HELD_PARROT, new HeldParrotDyamicRenderer());
        EntityRendererRegistry.register(Reg.ENTITY_THROWN_PARROT, ThrownParrotEntityRenderer::new);
        ModelLoadingRegistry.INSTANCE.registerVariantProvider(manager -> (modelid,ctx) -> {
            if (STOLEN_TRANSFORM == null) {
                try {
                    JsonUnbakedModel i  = JsonUnbakedModel.deserialize(new InputStreamReader(manager
                    .getResource(new Identifier("models/block/grass_block.json")).getInputStream()));
                    STOLEN_TRANSFORM = i.getTransformations();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (!modelid.getNamespace().equals(ModInit.MODID)) return null;
            if (modelid.equals(modelIdentifiers[0])) {
                try {
                    return getBirdNet(manager);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        });
    }

    
}