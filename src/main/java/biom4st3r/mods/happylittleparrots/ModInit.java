package biom4st3r.mods.happylittleparrots;

import org.jetbrains.annotations.Nullable;

import net.fabricmc.api.ModInitializer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.ProjectileDamageSource;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

public class ModInit implements ModInitializer {
	public static final class DamageSourceExtension extends ProjectileDamageSource {

		public DamageSourceExtension(Entity projectile, @Nullable Entity attacker) {
			super("parrot", projectile, attacker);
			this.setExplosive();
		}

		@Override
		public Text getDeathMessage(LivingEntity entity) {
			if (this.source.hasCustomName()) {
				if (this.getAttacker() == null) { // Player was blown up by a...Parrot?
					return new TranslatableText("death.parrot.explosion_nosource.named", entity.getDisplayName(), this.source.getDisplayName());
	
				} else if (entity == this.getAttacker()) { // Player blew themselfs up with...Parrots?
					return new TranslatableText("death.parrot.explosion_self.named", entity.getDisplayName(), this.source.getDisplayName());
				} else {
					return new TranslatableText("death.parrot.explosion_other.named", entity.getDisplayName(), this.getAttacker().getDisplayName(), this.source.getDisplayName());
				}
			}

			if (this.getAttacker() == null) { // Player was blown up by a...Parrot?
				return new TranslatableText("death.parrot.explosion_nosource", entity.getDisplayName());

			} else if (entity == this.getAttacker()) { // Player blew themselfs up with...Parrots?
				return new TranslatableText("death.parrot.explosion_self", entity.getDisplayName());
			} else {
				return new TranslatableText("death.parrot.explosion_other", entity.getDisplayName(), this.getAttacker().getDisplayName());
			}
		}
	}

	/** TODO
	 * 
	 * Explosion on collision(unless water, wool, bed, slime, honey)
	 */
	public static final String MODID = "happylittleparrots";

	@Override
	public void onInitialize() {
		Reg.classLoad();
	}
}
