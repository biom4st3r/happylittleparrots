package biom4st3r.mods.happylittleparrots.items;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;

import biom4st3r.mods.happylittleparrots.Reg;
import biom4st3r.mods.happylittleparrots.items.IParrotItemComponent.Power;
import net.minecraft.entity.Entity.RemovalReason;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.passive.ParrotEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Rarity;

public class BirdNetItem extends Item {
    private ImmutableMultimap<EntityAttribute, EntityAttributeModifier> mods;

    public BirdNetItem() {
        super(new Item.Settings().maxDamage(175).rarity(Rarity.COMMON).group(ItemGroup.TOOLS));
        this.mods = ImmutableMultimap.<EntityAttribute, EntityAttributeModifier>builder()
            .put(
                EntityAttributes.GENERIC_ATTACK_DAMAGE, 
                new EntityAttributeModifier(
                    ATTACK_DAMAGE_MODIFIER_ID, 
                    "Weapon modifier", 
                    -0.9, 
                    EntityAttributeModifier.Operation.MULTIPLY_TOTAL))
            .build();
        
    }

    @Override
    public Multimap<EntityAttribute, EntityAttributeModifier> getAttributeModifiers(EquipmentSlot slot) {
        return slot == EquipmentSlot.MAINHAND ? this.mods : super.getAttributeModifiers(slot);
    }

    @Override
    public boolean postHit(ItemStack stack, LivingEntity target, LivingEntity attacker) {
        int dmg = 1;
        if (target instanceof ParrotEntity parrot) {
            ItemStack is = new ItemStack(Reg.ITEM_HELD_PARROT);
            Reg.KEY_HELD_PARROT.borrowPooledComponent(is, comp -> {
                comp.set(parrot.getVariant(), Power.NO, parrot.getName().asString());
                // comp.variant = parrot.getVariant();
                // comp.name = parrot.getName().asString();
                // comp.power = Power.NO;
                parrot.remove(RemovalReason.DISCARDED);
            }, true);
            if (attacker instanceof PlayerEntity pe) {
                pe.giveItemStack(is);
            } 
        } else {
            dmg = 5;
        }
        stack.damage(dmg, attacker, e -> e.sendEquipmentBreakStatus(EquipmentSlot.MAINHAND));
        return true;
    }
    
    
}
