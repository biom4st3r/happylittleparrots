package biom4st3r.mods.happylittleparrots.items;

import biom4st3r.libs.biow0rks.PooledItemComponent;
import biom4st3r.mods.happylittleparrots.Reg;
import net.minecraft.item.ItemStack;

public class IParrotItemComponent extends PooledItemComponent {
    public static enum Power {
        BOOOOM(null),
        BOOOM(BOOOOM),
        BOOM(BOOOM),
        BOM(BOOM),
        NO(BOM),
        ;
        public final Power next;

        Power(Power power) {
            if (power == null) {
                power = this;
            }
            this.next = power;
        }
    }

    public IParrotItemComponent() {
        this.clear();
    }

    private int variant ;
    private Power power;
    private String name;

    public void set(int var, Power power, String name) {
        this.variant = var;
        this.setPower(power);
        this.name = name;
    }

    public Power getPower() {
        return this.power;
    }

    public String getName() {
        return this.name;
    }

    public void setPower(Power power) {
        this.power = power;
    }

    @Override
    public void fromStack(ItemStack stack) {
        if (stack.getSubNbt(getKey().id) == null) return;
        this.variant = stack.getSubNbt(getKey().id).getInt("var");
        this.power = Power.values()[stack.getSubNbt(getKey().id).getInt("power")];
        this.name = stack.getSubNbt(getKey().id).getString("name");
        
    }

    @Override
    public void toStack(ItemStack stack) {
        stack.getOrCreateSubNbt(getKey().id).putInt("var", this.variant);
        stack.getOrCreateSubNbt(getKey().id).putInt("power", this.power.ordinal());
        stack.getOrCreateSubNbt(getKey().id).putString("name", this.name);
    }

    @Override
    public void clear() {
        variant = -1;
        name = "";
        power = Power.NO;
    }

    @Override
    public ComponentKey<? extends PooledItemComponent> getKey() {
        return Reg.KEY_HELD_PARROT;
    }

    @Override
    public boolean validItem(ItemStack i) {
        return i.getItem() == Reg.ITEM_HELD_PARROT;
    }

    public int getVariant() {
        return variant;
    }
    
}
