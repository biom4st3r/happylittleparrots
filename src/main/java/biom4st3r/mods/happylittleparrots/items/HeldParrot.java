package biom4st3r.mods.happylittleparrots.items;

import java.util.List;

import biom4st3r.mods.happylittleparrots.Reg;
import biom4st3r.mods.happylittleparrots.entity.ThrownParrotEntity;
import biom4st3r.mods.happylittleparrots.items.IParrotItemComponent.Power;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.Entity.RemovalReason;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.passive.ParrotEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.StackReference;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.screen.slot.Slot;
import net.minecraft.sound.SoundCategory;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.ClickType;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class HeldParrot extends Item {

    public HeldParrot() {
        super(new Item.Settings().maxCount(8).group(ItemGroup.COMBAT));
    }
    
    public static class Ref<T> {
        public T ref;
        public boolean present() {return ref != null;};
    }

    public static void removeItemEntity(ItemEntity item) {
        if (item.world.isClient) return;
        if (item.getStack().getItem() == Reg.ITEM_HELD_PARROT) {
            for(int i = 0; i < item.getStack().getCount(); i ++) {
                HeldParrot.makeParrot(item.world, item.getStack(), item.getPos(), item.getVelocity());
            }
            item.remove(RemovalReason.DISCARDED);
        }
    }

    public enum Variant {
        RED,
        BLUE,
        GREEN,
        CYAN,
        GRAY,
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        if (world.isClient) return TypedActionResult.success(user.getStackInHand(hand));
        Ref<ThrownParrotEntity> ref = new Ref<>();
        final ItemStack handStack  = user.getStackInHand(hand);
        Reg.KEY_HELD_PARROT.borrowPooledComponent(handStack, comp -> {
            String name = handStack.hasCustomName() ? handStack.getName().asString() : comp.getName();
            ref.ref = new ThrownParrotEntity(world, comp.getVariant(), name, comp.getPower(), user);
            
        }, true);

        ItemStack is = user.getStackInHand(hand);
        is.decrement(1);

        if (ref.present()) {
            ref.ref.prevX = user.getX();
            ref.ref.prevY = user.getY();
            ref.ref.prevZ = user.getZ();
            float velocity = 1.5F;
            if (ref.ref.getVariant() == Variant.BLUE.ordinal()) {
                velocity *= 2;
            } else if (ref.ref.getVariant() == Variant.GRAY.ordinal()) {
                velocity = 1.0F;
            }
            ref.ref.refreshPositionAndAngles(user.getX(), user.getEyeY() - 0.1F, user.getZ(), user.getYaw(), user.getPitch());
            ref.ref.setVelocity(user, user.getPitch(), user.getYaw(), 0.0F, velocity, 1.0F);
            
            if (!ParrotEntity.imitateNearbyMob(world, ref.ref)) {
                world.playSound((PlayerEntity)null, user.getX(), user.getY(), user.getZ(), ParrotEntity.getRandomSound(world, world.random), SoundCategory.NEUTRAL, 1.5F, ParrotEntity.getSoundPitch(world.random));
            }
            world.spawnEntity(ref.ref);

            return TypedActionResult.success(is, true);
        }
        return TypedActionResult.fail(is);
    }

    public static void makeParrot(World world, ItemStack stack, Vec3d pos, Vec3d velocity) {
        ParrotEntity entity = new ParrotEntity(EntityType.PARROT, world);
        entity.refreshPositionAndAngles(pos.getX(), pos.getY(), pos.getZ(), 0, 0);
        Reg.KEY_HELD_PARROT.borrowPooledComponent(stack, comp -> {
            entity.setVariant(comp.getVariant());
            String name = stack.hasCustomName() ? stack.getName().asString() : comp.getName();
            if (!name.isBlank()) {
                entity.setCustomName(new LiteralText(comp.getName()));
            }
        }, false);
        entity.setVelocity(velocity);
        world.spawnEntity(entity);
    }


    @Override
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
        Reg.KEY_HELD_PARROT.borrowPooledComponent(stack, comp -> {
            if (!comp.getName().isBlank()) tooltip.add(new LiteralText(comp.getName()));
            if (comp.getPower() != Power.NO) {
                tooltip.add(new LiteralText(comp.getPower().name()));
            }
        }, false);
    }

    @Override
    public boolean onClicked(ItemStack parrot, ItemStack gunpowder, Slot slot, ClickType clickType, PlayerEntity player, StackReference cursorStackReference) {
        if (clickType != ClickType.RIGHT) return false;
        if (gunpowder.getItem() != Items.GUNPOWDER) return false;
        ItemStack newParrot = parrot.copy();
        newParrot.setCount(1);
        Reg.KEY_HELD_PARROT.borrowPooledComponent(newParrot, comp -> {
            comp.setPower(comp.getPower().next);
        }, true);
        parrot.decrement(1);
        player.giveItemStack(newParrot);
        gunpowder.decrement(1);
        return true;
    }
}
