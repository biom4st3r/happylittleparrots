package biom4st3r.mods.happylittleparrots.items;

import biom4st3r.mods.happylittleparrots.Reg;
import net.fabricmc.fabric.api.client.rendering.v1.BuiltinItemRendererRegistry.DynamicItemRenderer;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.render.model.json.ModelTransformation.Mode;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.passive.ParrotEntity;
import net.minecraft.item.ItemStack;

public class HeldParrotDyamicRenderer implements DynamicItemRenderer {

    public static ParrotEntity DUMMY;

    @Override
    @SuppressWarnings({"resource"})
    public void render(ItemStack stack, Mode mode, MatrixStack matrices, VertexConsumerProvider vertexConsumers,
            int light, int overlay) {
        if ((DUMMY == null && MinecraftClient.getInstance().world != null) || MinecraftClient.getInstance().world != DUMMY.world) {
            DUMMY = new ParrotEntity(EntityType.PARROT, MinecraftClient.getInstance().world);
        }
        if (!stack.hasNbt()) {
            DUMMY.setVariant(0);
        } else {
            Reg.KEY_HELD_PARROT.borrowPooledComponent(stack, comp -> {
                DUMMY.setVariant(comp.getVariant());
            }, false);
        }
        EntityRenderer<? super ParrotEntity> renderer = MinecraftClient.getInstance().getEntityRenderDispatcher().getRenderer(DUMMY);
        matrices.translate(0.5, 0, 0.5);
        renderer.render(DUMMY, DUMMY.getYaw(), 0, matrices, vertexConsumers, light);
    }
    
}
