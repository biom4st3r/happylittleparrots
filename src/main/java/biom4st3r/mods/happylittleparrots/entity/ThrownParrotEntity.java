package biom4st3r.mods.happylittleparrots.entity;

import java.util.function.Supplier;

import org.jetbrains.annotations.Nullable;

import biom4st3r.mods.happylittleparrots.ModInit;
import biom4st3r.mods.happylittleparrots.Reg;
import biom4st3r.mods.happylittleparrots.ModInit.DamageSourceExtension;
import biom4st3r.mods.happylittleparrots.items.HeldParrot.Variant;
import biom4st3r.mods.happylittleparrots.items.IParrotItemComponent.Power;
import net.fabricmc.fabric.api.tag.TagFactory;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityPose;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.passive.ParrotEntity;
import net.minecraft.entity.projectile.thrown.ThrownEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.Packet;
import net.minecraft.network.packet.s2c.play.EntitySpawnS2CPacket;
import net.minecraft.tag.Tag;
import net.minecraft.tag.Tag.Identified;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Identifier;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec2f;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import net.minecraft.world.explosion.Explosion.DestructionType;

public class ThrownParrotEntity extends ThrownEntity {
    private static final TrackedData<Integer> VARIANT = DataTracker.registerData(ThrownParrotEntity.class, TrackedDataHandlerRegistry.INTEGER);

    private Power power;
    public int getVariant() {
        return this.dummy.getVariant();
    }

    public ThrownParrotEntity(World world, int variant, String name, Power power, @Nullable LivingEntity thrower) {
        super(Reg.ENTITY_THROWN_PARROT, world);
        
        if (!name.isBlank()) {
            dummy.setCustomName(new LiteralText(name));
            this.setCustomName(new LiteralText(name));
        }
        this.power = power;
        this.dataTracker.set(VARIANT, variant);
        this.setOwner(thrower);
        dummy.setPose(EntityPose.STANDING);
    }

    private Entity THAT = this;

    public ParrotEntity dummy = new ParrotEntity(EntityType.PARROT, THAT.world) {
        @Override
        public Vec3d getPos() {
            return THAT.getPos();
        }

        public int getVariant() {
            return MathHelper.clamp(THAT.getDataTracker().get(VARIANT), 0, 4);
        }

        @Override
        public Vec2f getRotationClient() {
            return THAT.getRotationClient();
        }

        @Override
        public Vec3d getRotationVector() {
            return THAT.getRotationVector();
        }

        @Override
        public BlockPos getBlockPos() {
            return THAT.getBlockPos();
        }

        @Override
        public ChunkPos getChunkPos() {
            return THAT.getChunkPos();
        }

        @Override
        public Vec3d getTrackedPosition() {
            return THAT.getTrackedPosition();
        }

        @Override
        public float getPitch() {
            return THAT.getPitch();
        }

        @Override
        public float getPitch(float tickDelta) {
            return THAT.getPitch(tickDelta);
        }
        @Override
        public float getYaw() {
            return THAT.getYaw();
        }

        @Override
        public float getYaw(float tickDelta) {
            return THAT.getYaw(tickDelta);
        }

        @Override
        public float getHeadYaw() {
            return THAT.getHeadYaw();
        }
    };

    public float getExplosionPower() {
        if (this.getVariant() == Variant.RED.ordinal() && this.power != Power.NO) {
            return Math.max(5 - this.power.ordinal() + 0.5F, 0);
        }
        return Math.max(4 - this.power.ordinal() - 0.5F, 0);
    }

    public static final Supplier<Tag<Block>> SOFT = new Supplier<Tag<Block>>() {
        Identified<Block> tag;
        @Override
        public Tag<Block> get() {
            if (tag == null) {
                tag = TagFactory.BLOCK.create(new Identifier(ModInit.MODID, "soft_blocks"));
            }
            return tag;
        }
        
    };

    @Override
    protected void onCollision(HitResult hitResult) {
        super.onCollision(hitResult);
        if (!this.world.isClient) {
            if (this.power == Power.NO || (hitResult instanceof BlockHitResult result && world.getBlockState(result.getBlockPos()).isIn(SOFT.get())) || this.isTouchingWater()) {
                ParrotEntity entity = new ParrotEntity(EntityType.PARROT, this.world);
                entity.refreshPositionAndAngles(this.getX(), this.getY(), this.getZ(), this.getYaw(), this.getPitch());
                entity.setVariant(this.dataTracker.get(VARIANT));
                this.discard();
                entity.setCustomName(this.getCustomName());
                this.world.spawnEntity(entity);
            } else {
                this.world.createExplosion(
                    this, 
                    new DamageSourceExtension(this, this.getOwner()), 
                    null, 
                    this.getX(), 
                    this.getY(), 
                    this.getZ(), 
                    this.getExplosionPower(), 
                    false, 
                    getDestructionType()
                    );
                this.discard();
            }
        }
    }

    private DestructionType getDestructionType() {
        if (!this.world.getGameRules().getBoolean(GameRules.DO_MOB_GRIEFING)) {
            return DestructionType.NONE;
        } else if (!this.world.getGameRules().getBoolean(Reg.THROWN_PARROTS_DO_BLOCK_DAMAGE)) {
            return DestructionType.NONE;
        } else if (this.getVariant() == Variant.GREEN.ordinal()) {
            return DestructionType.NONE;
        } else {
            return DestructionType.BREAK;
        }
    }

    @Override
    protected void initDataTracker() {
        this.dataTracker.startTracking(VARIANT, 0);
    }

    @Override
    protected void readCustomDataFromNbt(NbtCompound nbt) {
        this.dataTracker.set(VARIANT, nbt.getInt("var"));
        this.dummy.setCustomName(new LiteralText(nbt.getString("name")));
        this.power = Power.values()[nbt.getInt("power")];
    }

    @Override
    protected void writeCustomDataToNbt(NbtCompound nbt) {
        nbt.putInt("var", this.dummy.getVariant());
        nbt.putInt("power", this.power.ordinal());
        nbt.putString("name", this.dummy.getName().asString());
    }

    @Override
    public Packet<?> createSpawnPacket() {
        return new EntitySpawnS2CPacket(this);
    }

    @Override
    protected float getGravity() {
        if (this.getVariant() == Variant.GRAY.ordinal()) {
            return 0.01F;
        } else if (this.getVariant() == Variant.RED.ordinal()) {
            return 0.2F;
        }
        return 0.1F;
    }
    
}
