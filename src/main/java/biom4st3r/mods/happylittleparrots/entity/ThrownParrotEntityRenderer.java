package biom4st3r.mods.happylittleparrots.entity;

import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.render.entity.ParrotEntityRenderer;
import net.minecraft.client.render.entity.EntityRendererFactory.Context;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;

public class ThrownParrotEntityRenderer extends EntityRenderer<ThrownParrotEntity> {

    ParrotEntityRenderer renderer;

    public ThrownParrotEntityRenderer(Context ctx) {
        super(ctx);
        renderer = new ParrotEntityRenderer(ctx);
    }

    @Override
    public void render(ThrownParrotEntity entity, float yaw, float tickDelta, MatrixStack matrices,
            VertexConsumerProvider vertexConsumers, int light) {
        
        renderer.render(entity.dummy, yaw, tickDelta, matrices, vertexConsumers, light);
    }

    @Override
    public Identifier getTexture(ThrownParrotEntity entity) {
        return null;
    }
    
}
